require 'rails_helper.rb'

feature  'Creating vehicles' do
  scenario 'can create vehicles' do
    #routes
    visit '/'
    #create vehicle
    click_link 'Nuevo vehículo'
    #fill form
    fill_in 'vehicle_vehicle_identifier', with: 'HA-3452'
    fill_in 'vehicle_sent_at', with: '2016-06-02 20:45:00'
    fill_in 'vehicle_locations_attributes_0_latitude', with: '20.23'
    fill_in 'vehicle_locations_attributes_0_longitude', with: '-0.56'
    #submit
    click_button 'Crear vehículo'
  end
end
