class Api::V1::VehiclesController < Api::V1::ApplicationController

  before_action :set_action, only: [:show]

  def create
    @vehicle = Vehicle.new(require_params)
    respond_to do |format|
      if @vehicle.save
        VehicleWorker.perform_async(@vehicle.id)
        format.json { render :show, status: :created, location: @vehicle }
      else
        format.json { render json: @vehicle.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
  end

  private

  def set_action
    @vehicle = Vehicle.find(params[:id])
  end

  def require_params
    params.require(:vehicle).permit(:id, :vehicle_identifier, :sent_at,
    locations_attributes:[:vehicle_id, :longitude,:latitude])
  end
end
