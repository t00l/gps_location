class VehiclesController < ApplicationController

   before_action :set_action, only: [:show]

  def index
    @vehicles = Vehicle.all
    @hash = Gmaps4rails.build_markers(@vehicles) do |vehicle, marker|
      vehicle_path = view_context.link_to vehicle.vehicle_identifier, vehicle_path(vehicle)
      marker.infowindow render_to_string(partial: "vehicles/infowindows", locals: {vehicle: vehicle, vehicle_path: vehicle_path})
      marker.lat vehicle.locations.last.latitude
      marker.lng vehicle.locations.last.longitude
      marker.picture({
        "url" => view_context.image_path("http://yava.ro/icons/car.png"),
        "width" => 32,
        "height" => 37
      })
    end
  end

  def new
    @vehicle = Vehicle.new
    @vehicle.locations.build
  end

  def create
    @vehicle = Vehicle.new(require_params)
    respond_to do |format|
      if @vehicle.save
        format.html { redirect_to @vehicle, notice: 'Vehículo creado con éxito.' }
        format.json { render :show, status: :created, location: @vehicle }
      else
        format.html { render :new }
        format.json { render json: @vehicle.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
    @hash = Gmaps4rails.build_markers(@vehicle) do |vehicle, marker|
      marker.lat vehicle.locations.last.latitude
      marker.lng vehicle.locations.last.longitude
      marker.infowindow render_to_string(partial: "vehicles/infowindows", locals: {vehicle: vehicle, vehicles_path: vehicles_path})
      marker.picture({
        "url" => view_context.image_path("http://yava.ro/icons/car.png"),
        "width" => 32,
        "height" => 37
      })
    end
  end

  private

  def set_action
    @vehicle = Vehicle.find(params[:id])
  end

  def require_params
    params.require(:vehicle).permit(:id, :vehicle_identifier, :sent_at,
    locations_attributes:[:vehicle_id, :longitude,:latitude])
  end
end
