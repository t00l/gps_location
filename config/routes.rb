Rails.application.routes.draw do

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'vehicles#index'

  resources :vehicles do
    resources :locations, only: [:create]
  end

  namespace :api , defaults: {format: :json} do
    namespace :v1 do
      resources :vehicles, only: [:show, :create],:path => 'gps' do
      end
    end
  end
end
