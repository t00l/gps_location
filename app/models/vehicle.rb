class Vehicle < ApplicationRecord
  has_many :locations

  validates :vehicle_identifier, uniqueness: true

  accepts_nested_attributes_for :locations, allow_destroy: true

  reverse_geocoded_by :latitude, :longitude

end
