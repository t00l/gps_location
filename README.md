# Beetrack
Create vehicles with their endpoints (latitude and longitude) and show them on google maps. A vehicle can have many endpoints, but in the map only shows the last one.
The system also provides a API in which you can add new vehicles and their endpoints, using the route 'api/v1/gps'.

## Github page:
* https://bitbucket.org/t00l/gps_location

## Gems included
* geocoder
* gmaps4rails
* bootstrap
* sidekiq
* rack-cors
* rspec-rails

## Included models
* vehicle
* location

## Other functionalities
* The index contains all the vehicles and their last endpoints
* You can change/add the endpoints in the details of the vehicle.

## Installation
```RUBY
rake db:migrate
bundle install
```
