class LocationsController < ApplicationController
  before_action :set_vehicle
  def new
    @location = @vehicle.locations.build
  end

  def create
    @location = @vehicle.locations.build(require_params)
    @vehicle.id = @location.vehicle_id
    respond_to do |format|
      if @location.save
        format.html { redirect_to @vehicle, notice: 'Vehículo creado con éxito.' }
        format.json { render :show, status: :created, location: @location }
      else
        format.html { render :new }
        format.json { render json: @vehicle.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  def require_params
    params.require(:location).permit(:vehicle_id, :longitude,:latitude)
  end

  def set_vehicle
    @vehicle = Vehicle.find params[:vehicle_id]
  end

end
