root = exports ? this
root.Vehicle = ( ->
	handler = ''
	init = (page,data) ->
		if page == 'index'
			gmaps_vehicle_index(data)
		if page == 'show'
			gmaps_vehicle_show(data)

	gmaps_vehicle_show = (data) ->
		handler = Gmaps.build('Google')
		handler.buildMap {
		  provider: {}
		  internal: id: 'map'
		}, ->
		  markers = handler.addMarkers(data);
		  handler.bounds.extendWith markers
		  handler.fitMapToBounds()
		  handler.getMap().setZoom 16
		  return

	gmaps_vehicle_index = (data) ->
		handler = Gmaps.build('Google')
		handler.buildMap {
		  provider: disableDefaultUI: true
		  internal: id: 'map'
		}, ->
		  markers = handler.addMarkers(data)
		  handler.bounds.extendWith markers
		  handler.fitMapToBounds()
		  return

	displayOnMap = (position) ->
	  markers = handler.addMarker(
	    lat: position.coords.latitude
	    lng: position.coords.longitude
	    picture:
	      url: 'http://yava.ro/icons/car.png'
	      width: 34
	      height: 34
	    infowindow: 'usuario')
	  handler.map.centerOn markers
	  handler.bounds.extendWith(markers);
	  return

	displayOnMapError = (position, handler) ->
	  marker2 = handler.addMarker(
	    lat: -33.469120
	    lng: -70.641997
	    picture:
	      url: 'http://www.inventicons.com/icons/uploads/iconset/1283/wm-16-Pin%20Remove-2.png'
	      width: 36
	      height: 36)
	  handler.map.centerOn marker2
	  handler.getMap().setZoom 15
	  handler.bounds.extendWith markers
	  handler.fitMapToBounds()
	  return

	{init: init}
	)()
