class Location < ApplicationRecord
  belongs_to :vehicle, optional: true

  #geocode
  reverse_geocoded_by :latitude, :longitude
 # auto-fetch address
end
